IMAGE = registry.gitlab.com/poloper/tools/weboob

build:
	docker build --pull -t ${IMAGE}:1.5 1.5
	docker build --pull -t ${IMAGE} 1.5

publish: build
	docker push ${IMAGE}:1.5
	docker push ${IMAGE}
